<h1>Documentação para o script java7/ireports</h1>

por João Guilherme Monteiro Reis 

O objetivo do script é auxiliar o desenvolvedor a deixando o processo de instalação do java mais simples possível, sem mais enrolação vamos ao passo a passo.
primeiro é bom realizar uma verificação para ver se tem o java instalado na maquina com os comandos: 

    cd /usr/lib  

    ls -l | jvm 

Após a verificação se tiver com a pasta do jvm la é recomendavel apagar com o comando 

    rm -rf jvm 

<h2> ATENÇÃO</h2>

*esse comando é apaga os diretórios de forma recursiva então se erra ele por uma besteira que seja pode apagar de vez algo importante do seu sistema , recomendo para nao ter nenhum problema seguir passo a passo esse script.*

Pronto após realizar a verificação do java vamos começar o tutorial. 

Na pasta do projeto vai contar um script linux de nome <java7.sh> , um diretório contendo o java7 e outro diretorio contendo um o ireport 4.7.1 é de suma importância que seja exatamente as versões que está nesse tutorial. 

Entre na pasta do projeto e utilize o comando 

    chmod  +x java7.sh

Em seguida der o comando 

    sudo ./java7.sh 

Após a execução do script abra digite o comando para abrir o arquivo de configuração do sistema

    sudo nano /etc/profile 

Em seguida esse codigo dentro do arquivo,lembrando que funcionar corretamente tem colocar extamente em cima do arquivo 

    JAVA_HOME=/usr/lib/jvm/java-oracle7
    PATH=$JAVA_HOME/bin:$PATH export PATH JAVA_HOME
    CLASSPATH=$JAVA_HOME/lib/tools.jar
    CLASSPATH=.:$CLASSPATH
    export  JAVA_HOME  PATH  CLASSPATH

Após a finalização do script reinicie a sua maquina normalmente com o comando 

    reboot 

Após a maquina reiniciar abra o terminal e verifique se o java encontra-se instalado com o comando

    java -version 

se a saida dele for algo tipo assim :

    java version "1.7.0_80"
    Java(TM) SE Runtime Environment (build 1.7.0_80-b15)
    Java HotSpot(TM) 64-Bit Server VM (build 24.80-b11, mixed mode)

o java instalou corretamente. 

ABRIR O IREPORT 

Após a instação do java der tudo correto, abra a pasta do ireport ( lembrando de extrair ele) e ir na pasta bin 

na pasta bin voce vai abrir o terminal e realizar os seguintes passos :

    chmod +x ireport 

    ./ireport 

pronto apos isso o programa vai abrir normalmente e só voce utilizar normal 🙂